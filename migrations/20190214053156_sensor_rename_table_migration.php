<?php


use Phinx\Migration\AbstractMigration;

class SensorRenameTableMigration extends AbstractMigration
{

    public function up() {
        $this->query('RENAME TABLE `sensor_temperatures` TO `sensor_float`');
    }

    public function down() {
        $this->query('RENAME TABLE `sensor_float` TO `sensor_temperatures`');
    }
}
