<?php


use Phinx\Migration\AbstractMigration;

class SensorNumericMigration extends AbstractMigration {
    public function up() {
        $query = 'CREATE TABLE `sensor_integer` ( 
            `id` int(11) NOT NULL AUTO_INCREMENT, 
            `uuid` varchar(120) NOT NULL, 
            
            `created` datetime NULL DEFAULT NULL,
            `modified` datetime NULL DEFAULT NULL,
            
            `last_value` int(11) NOT NULL DEFAULT 0,
            
            `value_count` int(11) NOT NULL DEFAULT 1,
            `value_sum` int(11) NOT NULL DEFAULT 0,
            `min_value` int(11) NOT NULL DEFAULT 0,
            `max_value` int(11) NOT NULL DEFAULT 0,          

            PRIMARY KEY (`id`),  
            KEY (`uuid`),
            KEY (`uuid`, `created`)  
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8';
        $this->query($query);
    }

    public function down() {
        $this->query('DROP TABLE `sensor_integer`');
    }
}
