<?php


use Phinx\Migration\AbstractMigration;

class AggregateSesnorsMigration extends AbstractMigration
{
    public function up() {
        $this->query('ALTER TABLE `sensor_float` ADD COLUMN `agregated` TINYINT NOT NULL DEFAULT 0, ADD KEY agregate_f (`agregated`)');
        $this->query('ALTER TABLE `sensor_integer` ADD COLUMN `agregated` TINYINT NOT NULL DEFAULT 0, ADD KEY agregate_i (`agregated`)');
    }

    public function down() {
        $this->query('ALTER TABLE `sensor_float` DROP KEY agregate_f, DROP COLUMN `agregated`');
        $this->query('ALTER TABLE `sensor_integer` DROP KEY agregate_i, DROP COLUMN `agregated`');
    }
}
