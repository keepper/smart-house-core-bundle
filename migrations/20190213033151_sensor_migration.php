<?php


use Phinx\Migration\AbstractMigration;

class SensorMigration extends AbstractMigration {
    public function up() {
        $query = 'CREATE TABLE `sensor_temperatures` ( 
            `id` int(11) NOT NULL AUTO_INCREMENT, 
            `uuid` varchar(120) NOT NULL, 
            
            `created` datetime NULL DEFAULT NULL,
            `modified` datetime NULL DEFAULT NULL,
            
            `last_value` decimal(10,2) NOT NULL DEFAULT 0,
            
            `value_count` int(11) NOT NULL DEFAULT 1,
            `value_sum` decimal(10,2) NOT NULL DEFAULT 0,
            `min_value` decimal(10,2) NOT NULL DEFAULT 0,
            `max_value` decimal(10,2) NOT NULL DEFAULT 0,          

            PRIMARY KEY (`id`),  
            KEY (`uuid`),
            KEY (`uuid`, `created`)  
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8';
        $this->query($query);

        $this->query('CREATE TABLE `digital_sensors` (
            `id` int(11) NOT NULL AUTO_INCREMENT, 
            `uuid` varchar(120) NOT NULL, 
            
            `created` datetime NULL DEFAULT NULL,
            `modified` datetime NULL DEFAULT NULL,
            
            `value` tinyint NULL DEFAULT NULL,

            PRIMARY KEY (`id`),  
            KEY (`uuid`),
            KEY (`uuid`, `created`)  
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down() {
        $this->query('DROP TABLE `sensor_temperatures`');
        $this->query('DROP TABLE `digital_sensors`');
    }
}
