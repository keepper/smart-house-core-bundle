<?php
namespace Keepper\SmartHouseCoreBundle\Command;

use Keepper\SmartHouse\Core\Registry\RegistryInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UiSwitchCommand extends Command {
    use LoggerAwareTrait;

    /**
     * @var RegistryInterface
     */
    private $registry;

    public function __construct(
        RegistryInterface $registry
    ) {
        $this->registry = $registry;
        parent::__construct(null);
        $this->setLogger(new NullLogger());
    }

    protected function configure() {
        $this
            ->setName('smart:ui-switch')
            ->setDescription('Производит управление переключателями')
            ->setHelp('smart:ui-switch uuid cmd'."\n".'команды: on, off, toggle, state');

        $this->addArgument('uuid');
        $this->addArgument('cmd');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $uuid = $input->getArgument('uuid');
        if ( is_null($uuid) ) {
            $output->writeln('Не передан обязательный аргумент uuid'."\n");

            $switchs = $this->registry->getSwitchs();
            foreach ($switchs as $switch) {
                $output->writeln("\t".$switch->uuid());
            }
            return;
        }

        try {
            $switch = $this->registry->getSwitch($uuid);
        } catch (\Exception $e) {
            $output->writeln('Ошибка: '.$e->getMessage());
            return;
        }

        $cmd = $input->getArgument('cmd');
        $state = $switch->state();
        $output->writeln('Состояние переключателя '.$uuid.' '.($state ? 'Вкл' : 'Выкл'));
        switch ($cmd) {
            case 'state':
                break;
            case 'on':
                $output->writeln('Включение переключателя '.$uuid);
                $switch->turnOn();
                break;
            case 'off':
                $output->writeln('Выключение переключателя '.$uuid);
                $switch->turnOff();
                break;
            case 'toggle':
                $output->writeln('Переключение переключателя '.$uuid);
                $switch->toggle();
                break;
            default:
                $output->writeln('Ошибка: не корректная команда ('.$cmd.')');
                return;
        }
    }
}