<?php
namespace Keepper\SmartHouseCoreBundle\Command;

use Keepper\SmartHouse\Core\Registry\RegistryInterface;
use Keepper\SmartHouse\Core\Sensor\DigitalSensorInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UiSensorCommand extends Command {
    use LoggerAwareTrait;

    /**
     * @var RegistryInterface
     */
    private $registry;

    public function __construct(
        RegistryInterface $registry
    ) {
        $this->registry = $registry;
        parent::__construct(null);
        $this->setLogger(new NullLogger());
    }

    protected function configure() {
        $this
            ->setName('smart:ui-sensor')
            ->setDescription('Производит управление сенсорами')
            ->setHelp('smart:ui-sensor uuid');

        $this->addArgument('uuid');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $uuid = $input->getArgument('uuid');
        if ( is_null($uuid) ) {
            $output->writeln('Не передан обязательный аргумент uuid'."\n");

            $sensors = $this->registry->getSensors();
            foreach ($sensors as $sensor) {
                $output->writeln("\t".$sensor->uuid());
            }
            return;
        }

        try {
            $sensor = $this->registry->getSensor($uuid);
        } catch (\Exception $e) {
            $output->writeln('Ошибка: '.$e->getMessage());
            return;
        }

        $value = $sensor->getValue();
        if ($sensor instanceof DigitalSensorInterface) {
            $value = $value ? 'on' : 'off';
        }

        $output->writeln('Значение сенсора ('.$uuid.') '.$value);
    }
}