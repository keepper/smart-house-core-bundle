<?php
namespace Keepper\SmartHouseCoreBundle\Command;

use Keepper\SmartHouse\Core\Registry\RegistryInterface;
use Keepper\SmartHouse\Core\Sensor\DigitalSensorInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncCommand extends Command {
    use LoggerAwareTrait;

    /**
     * @var RegistryInterface
     */
    private $registry;

    public function __construct(
        RegistryInterface $registry
    ) {
        $this->registry = $registry;
        parent::__construct(null);
        $this->setLogger(new NullLogger());
    }

    protected function configure() {
        $this
            ->setName('smart:sync')
            ->setDescription('Производит опрос всех зарегистрированных устройств, для определения их состояния и значений сенсоров')
            ->setHelp('smart:ui-sync');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $switchs = $this->registry->getSwitchs();
        foreach ($switchs as $switch) {
            try {
                $state = $switch->state();
                $output->writeln('Переключатель '.$switch->uuid().' '.($state ? 'on' : 'off'));
            } catch (\Exception $e) {
                $output->writeln('Переключатель '.$switch->uuid().' Ошибка чтения состояния: '.$e->getMessage());
            }
        }

        $sensors = $this->registry->getSensors();
        foreach ($sensors as $sensor) {
            try {
                $value = $sensor->getValue();
                if ( $sensor instanceof DigitalSensorInterface ) {
                    $output->writeln('Сенсор '.$sensor->uuid().' '.($value ? 'on' : 'off'));
                    continue;
                }
                $output->writeln('Сенсор '.$sensor->uuid().' '.$value);
            } catch (\Exception $e) {
                $output->writeln('Сенсор '.$sensor->uuid().' Ошибка чтения значения: '.$e->getMessage());
            }
        }
    }
}