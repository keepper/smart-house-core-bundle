<?php
namespace Keepper\SmartHouseCoreBundle\Command;

use Keepper\SmartHouse\Core\Output\OutputSensorInterface;
use Keepper\SmartHouse\Core\Registry\RegistryInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UiOutputCommand extends Command {
    use LoggerAwareTrait;

    /**
     * @var RegistryInterface
     */
    private $registry;

    public function __construct(
        RegistryInterface $registry
    ) {
        $this->registry = $registry;
        parent::__construct(null);
        $this->setLogger(new NullLogger());
    }

    protected function configure() {
        $this
            ->setName('smart:ui-output')
            ->setDescription('Производит управление сенсорами с возможность записи')
            ->setHelp('smart:ui-output uuid content');

        $this->addArgument('uuid');
        $this->addArgument('content');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $uuid = $input->getArgument('uuid');
        if ( is_null($uuid) ) {
            $output->writeln('Не передан обязательный аргумент uuid'."\n");

            $sensors = $this->registry->getOutputs();
            foreach ($sensors as $sensor) {
                $output->writeln("\t".$sensor->uuid());
            }
            return;
        }

        try {
            /**
             * @var OutputSensorInterface $sensor
             */
            $sensor = $this->registry->getOutput($uuid);
        } catch (\Exception $e) {
            $output->writeln('Ошибка: '.$e->getMessage());
            return;
        }

        $content = $input->getArgument('content');
        if ( is_null($content) ) {
            $output->writeln('Не передано значение для записи');
            return;
        }
        $output->writeln('Запись значения ('.$content.') в сенсор ('.$uuid.')');
        $sensor->write($content);
    }
}