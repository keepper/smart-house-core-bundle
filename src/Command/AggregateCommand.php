<?php
namespace Keepper\SmartHouseCoreBundle\Command;

use Keepper\SmartHouseCoreBundle\Service\AggregateService;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AggregateCommand extends Command {
    use LoggerAwareTrait;

    private $aggregator;

    public function __construct(
        AggregateService $aggregator
    ) {
        parent::__construct(null);
        $this->aggregator = $aggregator;
        $this->setLogger(new NullLogger());
    }

    protected function configure() {
        $this
            ->setName('smart:aggregate')
            ->setDescription('Производит агрегация данных датчиков, ужимая старые записи')
            ->setHelp('smart:aggregate');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->aggregator->agregate();
    }
}