<?php
namespace Keepper\SmartHouseCoreBundle\Command;

use Keepper\SmartHouse\Core\Button\PressModeInterface;
use Keepper\SmartHouse\Core\Registry\RegistryInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UiButtonCommand extends Command {
    use LoggerAwareTrait;

    /**
     * @var RegistryInterface
     */
    private $registry;

    public function __construct(
        RegistryInterface $registry
    ) {
        $this->registry = $registry;
        parent::__construct(null);
        $this->setLogger(new NullLogger());
    }

    protected function configure() {
        $this
            ->setName('smart:ui-button')
            ->setDescription('Производит управление кнопками')
            ->setHelp('smart:ui-button uuid press [mode]');

        $this->addArgument('uuid');
        $this->addArgument('cmd');
        $this->addArgument('mode', null, '', PressModeInterface::SINGLE);
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $uuid = $input->getArgument('uuid');
        if ( is_null($uuid) ) {
            $output->writeln('Не передан обязательный аргумент uuid'."\n");

            $buttons = $this->registry->getButtons();
            foreach ($buttons as $button) {
                $output->writeln("\t".$button->uuid());
            }
            return;
        }

        try {
            $button = $this->registry->getButton($uuid);
        } catch (\Exception $e) {
            $output->writeln('Ошибка: '.$e->getMessage());
        }

        $cmd = $input->getArgument('cmd');
        if ($cmd != 'press') {
            $output->writeln('Ошибка: не корректная команда ('.$cmd.')');
            return;
        }

        $mode = $input->getArgument('mode');
        $output->writeln('Нажатие кнопки '.$uuid.' режим: '.$mode);
        $button->press($mode);

    }
}