<?php
namespace Keepper\SmartHouseCoreBundle;

use Keepper\ConventionSkeleton\SkeletonBundle;
use Keepper\SmartHouseCoreBundle\DependencyInjection\RegistryCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SmartHouseCoreBundle extends SkeletonBundle {
    public function build(ContainerBuilder $container) {
        parent::build($container);
        $container->addCompilerPass(new RegistryCompilerPass());
    }
}