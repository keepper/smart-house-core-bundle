<?php
namespace Keepper\SmartHouseCoreBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class RegistryCompilerPass implements CompilerPassInterface {
    const TAG = 'uuid-device';
    const ACTION_TAG = 'action';

    public function process(ContainerBuilder $container) {
        $registry = $container->getDefinition('SmartHouse.Core.Registry');

        $uuidServices = $container->findTaggedServiceIds(self::TAG);

        foreach ($uuidServices as $id => $tags) {
            $registry->addMethodCall(
                'attachUuid',
                [new Reference($id)]
            );
        }

        $uuidServices = $container->findTaggedServiceIds(self::ACTION_TAG);

        foreach ($uuidServices as $id => $tags) {
            $registry->addMethodCall(
                'attachAction',
                [new Reference($id)]
            );
        }
    }
}