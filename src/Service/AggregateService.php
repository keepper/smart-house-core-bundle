<?php
namespace Keepper\SmartHouseCoreBundle\Service;

use Keepper\Lib\Pdo\Interfaces\PdoInterface;
use Keepper\SmartHouseCoreBundle\Repository\SensorRepository;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;

class AggregateService {

    use LoggerAwareTrait;

    private $tables;

    private $pdo;

    private $hour;

    public function __construct(
        PdoInterface $pdo,
        array $tables,
        int $hourAgo
    ) {
        $this->tables = $tables;
        $this->pdo = $pdo;
        $this->hour = $hourAgo;
        $this->setLogger(new NullLogger());
    }

    public function agregate(string $table = null) {
        if ( is_null($table) ) {
            foreach ($this->tables as $table) {
                $this->agregate($table);
            }
            return;
        }

        $startFrom = new \DateTime('now - '.$this->hour.' hours');

        $repository = new SensorRepository($this->pdo, $table);
        $query = 'SELECT 
                    CONCAT(DATE_FORMAT(`created`, \'%Y-%m-%d %H\'),\':00:00\') as `created`, 
                    `uuid`, 
                    MAX(`modified`) as `modified`, 
                    MIN(`min_value`) as `min_value`, 
                    MAX(`max_value`) as `max_value`, 
                    SUM(`value_count`) as `value_count`, 
                    SUM(`value_sum`) as `value_sum`, 
                    ROUND(`value_sum`/`value_count`,2) as `last_value` 
                  FROM `'.$table.'` 
                  WHERE 
                    `modified` < :to
                    AND `agregated` = 0
                  GROUP BY 1,2
                  ';
        $this->pdo->beginTransaction();
        try {
            $this->pdo->prepare('SET sql_mode = \'\'')->execute();
            $statment = $this->pdo->prepare($query);
            $statment->execute(['to' => $startFrom->format('Y-m-d H:i:s')]);
        } catch (\Exception $e) {
            $this->pdo->rollBack();
            $this->logger->error('Ошибка исполнения запроса выбора не агрегированных данных с сенсоров'."\n".$e->getMessage()."\n".$query);
            return;
        }
        $rows = $statment->fetchAll();
        
        foreach ($rows as $row) {
            $query = 'INSERT INTO `'.$table.'` (`uuid`, `created`, `modified`, `min_value`, `max_value`, `value_count`, `value_sum`, `last_value`, `agregated`)
            VALUES (:uuid, :created, :modified, :min, :max, :count, :sum, :value, 1)';

            try {
                $statment = $this->pdo->prepare($query);
                $statment->execute([
                    'uuid' => $row['uuid'],
                    'created' => $row['created'],
                    'modified' => $row['modified'],
                    'min' => $row['min_value'],
                    'max' => $row['max_value'],
                    'sum' => $row['value_sum'],
                    'count' => $row['value_count'],
                    'value' => $row['last_value'],
                ]);
            } catch (\Exception $e) {
                $this->pdo->rollBack();
                $this->logger->error('Ошибка вставки агрегированных данных с сенсоров'."\n".$e->getMessage()."\n".$query);
                return;
            }
        }

        try {
            $statment = $this->pdo->prepare('DELETE FROM `'.$table.'` WHERE `modified` < :to AND `agregated` = 0');
            $statment->execute(['to' => $startFrom->format('Y-m-d H:i:s')]);
        } catch (\Exception $e) {
            $this->pdo->rollBack();
            $this->logger->error('Ошибка удаления не агрегированных данных сенсоров'."\n".$e->getMessage()."\n".$query);
            return;
        }

        $this->pdo->commit();
    }
}