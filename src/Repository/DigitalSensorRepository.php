<?php
namespace Keepper\SmartHouseCoreBundle\Repository;

use Keepper\SmartHouse\Core\Storage\StateStorageInterface;

class DigitalSensorRepository extends AbstractRepository implements StateStorageInterface {

    /**
     * @inheritdoc
     */
    public function getLastValue(string $uuid) {
        $value = $this->findFirst(['uuid = :uuid'], ['uuid' => $uuid], '`created` DESC');

        return is_null($value) ? null : $value['value'];
    }

    /**
     * @inheritdoc
     */
    public function saveValue(string $uuid, $value) {
        $record = $this->findFirst(['uuid = :uuid'], ['uuid' => $uuid], '`created` DESC');

        if (is_null($record) || $record['value'] != $value) {
            try {
                $this->addRecord(
                    [
                        'created' => 'now()',
                        'modified' => 'now()',
                        'value' => ':value',
                        'uuid' => ':uuid'
                    ], [
                    'value' => (int) $value,
                    'uuid' => $uuid
                ]);
            } catch (\PDOException $e) {
                $this->logger->error('Ошибка при добавлении записи в '.$this->tableName.' '.print_r([
                    'created' => 'now()',
                    'modified' => 'now()',
                    'value' => (int) $value,
                    'uuid' => $uuid
                ], true)."\n".$e->getTraceAsString());
                throw $e;
            }
        } else {
            try {
                $this->updateRecord(
                    ['id = :id'],
                    ['`modified`' => 'now()'],
                    ['id' => $record['id']]
                );
            } catch (\PDOException $e) {
                $this->logger->error('Ошибка при бновлении записи в '.$this->tableName.' ('.$record['id'].') '.print_r([
                        'modified' => 'now()'
                ], true)."\n".$e->getTraceAsString());
                throw $e;
            }
        }
    }

    protected function toDTO(array $rawRecord) {
        return $rawRecord;
    }
}