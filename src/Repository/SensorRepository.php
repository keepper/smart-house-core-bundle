<?php
namespace Keepper\SmartHouseCoreBundle\Repository;

use Keepper\Lib\Pdo\Interfaces\PdoInterface;
use Keepper\SmartHouse\Core\Storage\AnaliticStateStorageInterface;
use Keepper\SmartHouse\Core\Storage\AverageRecordInterface;

class SensorRepository extends AbstractRepository implements AnaliticStateStorageInterface {

    private $period;

    public function __construct(PdoInterface $pdo, string $tableName, int $period = 60) {
        parent::__construct($pdo, $tableName);
        $this->setPeriod($period);
    }

    protected function toDTO(array $rawRecord) {
        return new AverageRecord($rawRecord);
    }

    public function setPeriod(int $period) {
        $this->period = $period;
    }

    /**
     * @inheritdoc
     */
    public function getLastValue(string $uuid) {
        /**
         * @var AverageRecordInterface $value
         */
        $value = $this->findFirst(['uuid = :uuid'], ['uuid' => $uuid], '`created` DESC');

        return is_null($value) ? null : $value->lastValue();
    }

    /**
     * @inheritdoc
     */
    public function saveValue(string $uuid, $value) {
        $currentDate = new \DateTime('now');
        $secondsAgo = null;
        /**
         * @var AverageRecordInterface $previus
         */
        $previus = $this->findFirst(['uuid = :uuid'], ['uuid' => $uuid], '`created` DESC');
        if ( !is_null($previus) ) {
            $timeDiff = $currentDate->diff($previus->startedAt());
            $secondsAgo = $timeDiff->s + $timeDiff->i * 60 + $timeDiff->h * 60 * 60;
        }
        if ( is_null($previus) || $secondsAgo > $this->period) {
            $this->addRecord([
                '`created`' => 'now()',
                '`modified`'=> 'now()',
                '`last_value`' => $value,
                '`min_value`' => $value,
                '`max_value`' => $value,
                '`value_sum`' => $value,
                '`uuid`' => ':uuid',
                '`value_count`' => 1
            ],[
                'uuid' => $uuid
            ]);
        } else {
            $this->updateRecordById($previus->id(),[
                '`modified`'=> 'now()',
                '`last_value`' => $value,
                '`min_value`' => $value < $previus->minValue() ? $value : $previus->minValue(),
                '`max_value`' => $value > $previus->maxValue() ? $value : $previus->maxValue(),
                '`value_sum`' => $value . ' + `value_sum`',
                '`uuid`' => ':uuid',
                '`value_count`' => $previus->count() + 1
            ],[
                'uuid' => $uuid
            ]);
        }
    }

    public function getByUuid(string $uuid, \DateTimeInterface $from = null, \DateTimeInterface $to = null): ?AverageRecordInterface {
        if ( is_null($from) ) {
            return $this->findFirst(['uuid = :uuid'], ['uuid' => $uuid], '`created` DESC');
        }

        if ( is_null($to) ) {
            $to = new \DateTime('now');
        }

        $query = 'SELECT 
              min(`created`) as `created`,
              max(`modified`) as `modified`,
              sum(`value_sum`) as `value_sum`,
              sum(`value_count`) as `value_count`,
              min(`min_value`) as `min_value`,
              max(`max_value`) as `max_value`  
            FROM `'.$this->tableName.'`
            WHERE uuid = "'.$uuid.'" 
            AND `created` >= "'.$from->format('Y-m-d H:i:s').'"
            AND `modified` <= "'.$to->format('Y-m-d H:i:s').'"';

        $statment = $this->pdo->prepare($query);
        $statment->execute([]);

        $this->logger->debug($query.' rows:'.$statment->rowCount());
        if ($statment->rowCount() == 0) {
            return null;
        }

        $last = $this->findFirst([
            'uuid = :uuid',
            'created >= :created',
            'modified <= :modified'
        ], [
            'uuid' => $uuid,
            'created' => $from->format('Y-m-d H:i:s'),
            'modified'=> $to->format('Y-m-d H:i:s')
        ], '`created` DESC');

        $result = $statment->fetch(\PDO::FETCH_ASSOC);
        $result['uuid'] =$uuid;
        $result['id']   = 0;
        $result['last_value'] = $last->lastValue();

        return $this->toDTO($result);
    }
}