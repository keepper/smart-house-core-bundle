<?php
namespace Keepper\SmartHouseCoreBundle\Repository;

use Keepper\Lib\Pdo\Interfaces\PdoInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

abstract class AbstractRepository {

    use LoggerAwareTrait;

    protected $tableName;

    /**
     * @var PdoInterface
     */
    protected $pdo;

    public function __construct(PdoInterface $pdo, string $tableName) {
        $this->pdo = $pdo;
        $this->tableName = $tableName;
        $this->setLogger(new NullLogger());
    }

    public function setLogger(LoggerInterface $logger) {
        $this->logger = $logger;

        if (!($logger instanceof NullLogger)) {
            $this->pdo->setLogger($logger);
        }
    }

    public function findFirst(array $conditions = null, array $arguments = null, $order = null) {
        $query = 'SELECT * FROM `'.$this->tableName.'`';
        if ( !is_null($conditions) ) {
            $query.= ' WHERE '.implode(' AND ', $conditions);
        }
        if ( !is_null($order) ) {
            $query.= ' ORDER BY '.$order;
        }
        $query.= ' LIMIT 0,1';

        $statment = $this->pdo->prepare($query);
        $statment->execute($arguments);

        $this->logger->debug($query.' rows:'.$statment->rowCount());
        if ($statment->rowCount() == 0) {
            return null;
        }

        return $this->toDTO($statment->fetch(\PDO::FETCH_ASSOC));
    }

    public function findAll(array $conditions = null, array $arguments = null): array {
        $result = [];
        $query = 'select * from `'.$this->tableName.'`';
        if ( !is_null($conditions) ) {
            $query.= ' WHERE '.implode(' AND ', $conditions);
        }

        try {
            $statment = $this->pdo->prepare($query);
            $statment->execute($arguments);
        } catch (\Exception $e) {
            $this->logger->error($query);
            throw $e;
        }

        $this->logger->debug($query.' rows:'.$statment->rowCount());
        if ($statment->rowCount() == 0) {
            return $result;
        }

        $rows = $statment->fetchAll();
        foreach ($rows as $row) {
            $result[] = $this->toDTO($row);
        }
        return $result;
    }

    public function addRecord(array $data, array $arguments = []) {
        $keys = array_keys($data);
        $values = array_values($data);
        $query = 'INSERT INTO `'.$this->tableName.'`  '.
            '('.implode(', ', $keys).') VALUES ('.implode(', ', $values).')';

        $this->logger->debug($query);
        $statment = $this->pdo->prepare($query);
        $statment->execute($arguments);

        $id = $this->pdo->lastInsertId();
        return $this->findFirst(['id = '.$id]);
    }

    public function updateRecord(array $conditions, array $data, array $arguments = []) {
        $inLineData = [];
        foreach ($data as $key => $value) {
            $inLineData[] = $key.' = '.$value;
        }
        $query = 'UPDATE `'.$this->tableName.'` SET ' .implode(', ', $inLineData).' WHERE ';
        $query.= implode(' AND ', $conditions);

        $statment = $this->pdo->prepare($query);
        $statment->execute($arguments);
        $this->logger->debug($query.'; args='.print_r($arguments, true));
    }

    public function updateRecordById(int $id, array $data, array $arguments = []) {
        $arguments['id'] = $id;
        return $this->updateRecord(['id = :id'], $data, $arguments);
    }

    abstract protected function toDTO(array $rawRecord);
}