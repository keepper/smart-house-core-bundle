<?php
namespace Keepper\SmartHouseCoreBundle\Repository;

use Keepper\SmartHouse\Core\Storage\AverageRecordInterface;

class AverageRecord implements AverageRecordInterface {

    private $id;
    private $uuid;
    private $created;
    private $modified;
    private $lastValue;
    private $count;
    private $sum;
    private $min;
    private $max;

    public function __construct(array $raw)
    {
        $this->id = $raw['id'];
        $this->uuid = $raw['uuid'];
        $this->created = $raw['created'];
        $this->modified = $raw['modified'];
        $this->lastValue = $raw['last_value'];
        $this->count = $raw['value_count'];
        $this->sum = $raw['value_sum'];
        $this->min = $raw['min_value'];
        $this->max = $raw['max_value'];
    }

    public function id(): int {
        return $this->id;
    }

    public function lastValue(): float {
        return $this->lastValue;
    }

    public function startedAt(): \DateTimeInterface {
        return \DateTime::createFromFormat('Y-m-d H:i:s', $this->created);
    }

    public function endedAt(): \DateTimeInterface {
        return \DateTime::createFromFormat('Y-m-d H:i:s', $this->modified);
    }

    public function count(): int {
        return $this->count;
    }

    public function avg(): float {
        return $this->sum / $this->count();
    }

    public function minValue(): float {
        return $this->min;
    }

    public function maxValue(): float {
        return $this->max;
    }
}