# smart-house-core-bundle

## Регистрация uuid низкоуровневой абстракции от устройств поте тегу

Для регистрации uuid в Registry просто отметьте сервис реализующие интерфейс **UuidInterface** тегом **uuid-device**

## Консольные команды

### Управление кнопками

1. Получение списка uuid всех зарегистрированных кнопок

        bin/console smart:ui-button

1. Управление кнопкой

        bin/console smart:ui-button (uuid) (cmd) [(mode)]
    
Где:

 * uuid  идентификатор кнопки
 * cmd  команда
    - press - Нажатие кнопки
 * mode режим нажатия
    - 1 Обычное нажатие
    - 2 Двойное нажатие
    - 3 Длительное нажатие
    
### Управление переключателями

1. Получение списка uuid всех зарегистрированных переключателей

        bin/console smart:ui-switch
        
1. Получение состояния переключателя

        bin/console smart:ui-switch (uuid) state
        
1. Включение 

        bin/console smart:ui-switch (uuid) on
        
1. Выключение 

        bin/console smart:ui-switch (uuid) off

1. Переключение 

        bin/console smart:ui-switch (uuid) toggle


### Работа с сенсорами

1. Получение списка uuid всех зарегистрированных сенсоров

        bin/console smart:ui-sensor
        
1. Получение состояния сенсора

        bin/console smart:ui-sensor (uuid)
        
        
###  Синхронизация состояний всех переключателей и сенсоров

        bin/console smart:ui-sync
        

