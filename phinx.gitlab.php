<?php

return [
    'paths' => [
        'migrations' => 'migrations'
    ],
    'environments' => [
	    'default_migration_table' => 'db_versions',
	    'default_database'        => 'smart',
	    'smart'                   => [
		    'adapter' => 'mysql',
		    'host'    => 'mysql',
		    'name'    => 'smart',
		    'user'    => 'root',
		    'pass'    => 'mysql_strong_password',
		    'port'    => '3306',
		    'charset' => 'utf8',
	    ]
    ],
];
