<?php
namespace Keepper\SmartHouseCoreBundle\Tests;

use Keepper\SmartHouse\Core\Registry\RegistryInterface;
use Keepper\SmartHouseCoreBundle\SmartHouseCoreBundle;

class ServiceGetterTest extends TestCase {

    public function testGetRegistry() {
        $registry = $this->getService('SmartHouse.Core.Registry');
        $this->assertInstanceOf(RegistryInterface::class, $registry);
    }

    public function testTagedUuidDevice() {
        /**
         * @var RegistryInterface $registry
         */
        $registry = $this->getService('SmartHouse.Core.Registry');

        $registry->getButton('test-button');
    }
}