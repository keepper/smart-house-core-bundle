<?php
namespace Keepper\SmartHouseCoreBundle\Tests\Sensor;


use Keepper\SmartHouse\Core\Sensor\System\DiskSpaceSensor;
use Keepper\SmartHouseCoreBundle\Repository\SensorRepository;
use Keepper\SmartHouseCoreBundle\Tests\TestCase;

class DiskSpaceSensorTest extends TestCase {

    public function testReadFromSensor() {
        $repository = new SensorRepository(
            $this->getService('SmartHouse.Core.Pdo'),
            'sensor_integer',
            3600
        );
        $repository->setLogger($this->logger);

        $sensor = new DiskSpaceSensor(
            'test-disc-space', '/', $repository
        );
        $sensor->setLogger($this->logger);

        $this->assertNotNull($sensor->getValue());
    }
}