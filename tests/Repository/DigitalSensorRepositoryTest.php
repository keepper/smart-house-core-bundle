<?php
namespace Keepper\SmartHouseCoreBundle\Tests\Repository;

use Keepper\SmartHouse\Core\Storage\StateStorageInterface;
use Keepper\SmartHouseCoreBundle\Repository\AbstractRepository;
use Keepper\SmartHouseCoreBundle\Tests\TestCase;

class DigitalSensorRepositoryTest extends TestCase {
    public function testGetLastValue() {
        /**
         * @var StateStorageInterface $repository
         */
        $repository = $this->getService('SmartHouse.Repository.DigitalSensor');

        $result = $repository->getLastValue('test-uuid');
        $this->assertNull($result);

        $repository->saveValue('test-uuid', 1);
        $result = $repository->getLastValue('test-uuid');
        $this->assertEquals(1, $result);
    }

    public function testSaveValue() {
        /**
         * @var StateStorageInterface|AbstractRepository $repository
         */
        $repository = $this->getService('SmartHouse.Repository.DigitalSensor');

        $countBefore = count($repository->findAll(['uuid=:uuid'],['uuid'=>'test-uuid']));
        $this->assertEquals(0, $countBefore);

        $repository->saveValue('test-uuid', 1);
        $countAfter = count($repository->findAll(['uuid=:uuid'],['uuid'=>'test-uuid']));
        $this->assertEquals(1, $countAfter);

        $repository->saveValue('test-uuid', 1);
        $countAfter = count($repository->findAll(['uuid=:uuid'],['uuid'=>'test-uuid']));
        $this->assertEquals(1, $countAfter);

        $repository->saveValue('test-uuid', 0);
        $countAfter = count($repository->findAll(['uuid=:uuid'],['uuid'=>'test-uuid']));
        $this->assertEquals(2, $countAfter);
    }

    public function testSaveBoolean() {
        /**
         * @var StateStorageInterface $repository
         */
        $repository = $this->getService('SmartHouse.Repository.DigitalSensor');

        $repository->saveValue('test-uuid', '1');
        $result = $repository->getLastValue('test-uuid');
        $this->assertEquals(1, $result);

        $repository->saveValue('test-uuid', false);
        $result = $repository->getLastValue('test-uuid');
        $this->assertEquals(0, $result);
    }
}