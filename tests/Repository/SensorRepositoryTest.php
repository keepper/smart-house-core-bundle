<?php
namespace Keepper\SmartHouseCoreBundle\Tests\Repository;

use Keepper\SmartHouse\Core\Storage\StateStorageInterface;
use Keepper\SmartHouseCoreBundle\Repository\AbstractRepository;
use Keepper\SmartHouseCoreBundle\Repository\SensorRepository;
use Keepper\SmartHouseCoreBundle\Tests\TestCase;

class SensorRepositoryTest extends TestCase {

    public function testGetLastValue() {
        /**
         * @var StateStorageInterface $repository
         */
        $repository = $this->getService('SmartHouse.Repository.SensorFloat.OneMinute');

        $result = $repository->getLastValue('test-uuid');
        $this->assertNull($result);

        $repository->saveValue('test-uuid', 10.53);
        $result = $repository->getLastValue('test-uuid');
        $this->assertEquals(10.53, $result);
    }

    public function testDateDiff() {
        $date_1 = \DateTime::createFromFormat('Y-m-d H:i:s', '2019-02-13 12:25:56');
        $date_2 = \DateTime::createFromFormat('Y-m-d H:i:s', '2019-02-13 12:26:06');

        $diff = $date_2->diff($date_1);
        $this->assertEquals(10, $diff->s);
    }

    public function testSave() {
        /**
         * @var StateStorageInterface|SensorRepository $repository
         */
        $repository = $this->getService('SmartHouse.Repository.SensorFloat.OneMinute');
        $repository->setPeriod(3);

        $this->assertNull($repository->getByUuid('test-uuid'));

        $repository->saveValue('test-uuid', 9.3);
        $repository->saveValue('test-uuid', 20.56);
        $repository->saveValue('test-uuid', 10.23);

        $result = $repository->getByUuid('test-uuid');
        $this->assertEquals(3, $result->count());
        $this->assertEquals(9.3, $result->minValue());
        $this->assertEquals(20.56, $result->maxValue());
        $this->assertEquals(10.23, $result->lastValue());
        $this->assertEquals((9.3+20.56+10.23)/3, $result->avg());

        sleep(4);

        $repository->saveValue('test-uuid', 20.56);

        $result = $repository->getByUuid('test-uuid');
        $this->assertEquals(1, $result->count());
        $this->assertEquals(20.56, $result->minValue());
        $this->assertEquals(20.56, $result->maxValue());
        $this->assertEquals(20.56, $result->lastValue());
        $this->assertEquals(20.56, $result->avg());

        sleep(2);
        $agregate = $repository->getByUuid('test-uuid', new \DateTime('now - 10 seconds'));

        $this->assertEquals(4, $agregate->count());
        $this->assertEquals(9.3, $agregate->minValue());
        $this->assertEquals(20.56, $agregate->maxValue());
        $this->assertEquals(20.56, $agregate->lastValue());
        $this->assertEquals((9.3+20.56*2+10.23)/4, $agregate->avg());
    }

    public function testBugWithIncorrectUpdate() {
        /**
         * @var StateStorageInterface|SensorRepository $repository
         */
        $repository = $this->getService('SmartHouse.Repository.SensorFloat.OneMinute');
        $repository->setPeriod(1);

        $repository->saveValue('test-uuid-1', 9.3);
        $repository->saveValue('test-uuid-2', 20.56);
        $repository->saveValue('test-uuid-1', 10.23);

        $this->assertNotNull($repository->getByUuid('test-uuid-2'));
    }
}