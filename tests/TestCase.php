<?php
namespace Keepper\SmartHouseCoreBundle\Tests;

use Keepper\ConventionSkeleton\Tests\Symfony\ServiceTestCase;
use Keepper\Lib\Pdo\Interfaces\PdoInterface;
use Keepper\SmartHouse\Core\Registry\RegistryInterface;
use Keepper\SmartHouseCoreBundle\SmartHouseCoreBundle;

class TestCase extends ServiceTestCase {

    function baseParameters(array $addTo = []): array {
        return array_merge([
            'database_host' => env('database_host'),
            'database_name' => env('database_name'),
            'database_user' => env('database_user'),
            'database_password' => env('database_password'),
        ], $addTo);
    }

    // Регистрация Бандлов в ядре
    function bundleClassesToRegister(array $addTo = []): array {
        return parent::bundleClassesToRegister([
            SmartHouseCoreBundle::class,
        ]);
    }

    public function setUp() {
        parent::setUp();
        /**
         * @var PdoInterface $pdo
         */
        $pdo = $this->getService('SmartHouse.Core.Pdo');
        $pdo->beginTransaction();
    }

    public function tearDown() {
        /**
         * @var PdoInterface $pdo
         */
        $pdo = $this->getService('SmartHouse.Core.Pdo');
        $pdo->rollBack();

        parent::tearDown();
    }

}